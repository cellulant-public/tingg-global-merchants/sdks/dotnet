# Tingg Checkout SDK for .NET
[![tingg](https://cdn.cellulant.africa/images/brand-assets/tingg-by-cellulant.svg)](https://tingg.africa)

The Tingg Checkout SDK gives fast and easy access to the Tingg hosted checkout platform.

## Getting started

Explain how to use your package, provide clear and concise getting started instructions, including any necessary steps.

### Prerequisites

You need a [Tingg Account](https://app.uat.tingg.africa/cas/login) to use this package. If you don't have one you can contact our account managers through tingg-checkout@cellulant.io and have your business registered & activated.

Visit our [Official Documentation](https://docs.tingg.africa/docs/checkout-getting-started) to find out more on how you can get started using Tingg.

Once you're signed in, you will need to retrieve your [API Keys](https://docs.tingg.africa/docs/checkout-getting-started#4--checkout-api-keys), that is the IV Key, the Secret Key and the Access Key.

## Installation
```
dotnet add package Tingg.Checkout.Net
```

## Usage

1. To use this library add a using statement for Checkout.Models and Checkout.Service
2. You can now initialize CheckoutEncryption  passing in the relevant parameters IVKey and SecretKey (advised to
   have these keys in a separate configuration source)
3. Validate the payload by calling the CheckoutValidation.Validate() function.
4. Call Encrypt() on the CheckoutEncryption instance passing in the validated payload.

```c#
using Checkout.Models;
using Checkout.Service;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors();

// Add services to the container.
var app = builder.Build();


// Configure the HTTP request pipeline.

app.UseHttpsRedirection();
app.UseCors(x=>x
.SetIsOriginAllowed(origin=>true));

// Generate express link
app.MapPost("/checkout-express", (Payload payload) =>
{
    CheckoutExpress checkoutExpress = new CheckoutExpress(SupportedEnvironment.Testing);
    try {
        return checkoutExpress.create("<API_KEY>", "<CLIENT_ID>", "<CLIENT_SECRET>", payload);
    } catch (ValidationException ex) {
        Console.WriteLine($"Validation failed: {ex.Message}");
    } catch (Exception ex) {
        Console.WriteLine($"Encryption failed: {ex.Message}");
    }
});


// encrypt payload
app.MapPost("/checkout-encryption", (Payload payload) =>
{
    CheckoutEncryption checkoutEncryption = new CheckoutEncryption("<IV_KEY>", "<SECRET_KEY>");
    try {
        //OPTIONAL - Validate payload before encryption
        Payload validPayload = CheckoutValidation.Validate(payload);
        string encryptedParams = checkoutEncryption.encrypt(validPayload);
        return new CheckoutResponse() 
         {
            encrypted_payload = encryptedParams,
            access_key = "<ACCESS_KEY>"
         };
    } catch (ValidationException ex) {
        Console.WriteLine($"Validation failed: {ex.Message}");
    } catch (Exception ex) {
        Console.WriteLine($"Encryption failed: {ex.Message}");
    }
});

app.Run();
```

## Additional documentation

- [Official Documentation](https://docs.tingg.africa/docs/checkout-getting-started)

## Feedback

Feel free to reach us through our [discussion forum](https://docs.tingg.africa/discuss).
