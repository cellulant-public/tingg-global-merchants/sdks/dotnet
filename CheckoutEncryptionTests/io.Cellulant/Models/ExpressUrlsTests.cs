﻿using Checkout.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Checkout.Service.Tests
{

	[TestClass()]
	public class ExpressUrlsTests
	{

		[TestMethod()]
		public void testGetterAndSetters()
		{
			ExpressUrls expressUrls = new ExpressUrls
			{
                LongUrl = "https://long.url",
				ShortUrl = "https://short.url"
			};

			Assert.AreEqual("https://long.url", expressUrls.LongUrl);
			Assert.AreEqual("https://short.url", expressUrls.ShortUrl);
		}

	}
}

