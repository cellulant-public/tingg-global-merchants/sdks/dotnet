﻿using System;
using Checkout.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Checkout.Service.Tests
{
    [TestClass()]
    public class ConstantsTests
    {
        [TestMethod]
        public void ValidateAUTH_BASE_URLDictionary()
        {
            Assert.AreEqual(
                "https://api.tingg.africa/v1",
                Constants.AUTH_BASE_URL[SupportedEnvironment.Production]
            );
            Assert.AreEqual(
                "https://api-test.tingg.africa/v1",
                Constants.AUTH_BASE_URL[SupportedEnvironment.Testing]
            );
            Assert.AreEqual(
                "https://api-approval.tingg.africa/v1",
                Constants.AUTH_BASE_URL[SupportedEnvironment.Sandbox]
            );
        }

        [TestMethod]
        public void ValidateAPI_BASE_URLDictionary()
        {
            Assert.AreEqual(
                "https://api.tingg.africa/v1",
                Constants.API_BASE_URL[SupportedEnvironment.Production]
            );
            Assert.AreEqual(
                "https://api-approval.tingg.africa/v1",
                Constants.API_BASE_URL[SupportedEnvironment.Sandbox]
            );
            Assert.AreEqual(
                "https://online.uat.tingg.africa/testing",
                Constants.API_BASE_URL[SupportedEnvironment.Testing]
            );
        }

        [TestMethod]
        public void ValidateAPI_ENDPOINTSDictionary()
        {
            Assert.AreEqual("/oauth/token/request", Constants.API_ENDPOINTS[ApiEndpoint.Auth]);
            Assert.AreEqual(
                "/request-service/checkout-request/express-request",
                Constants.API_ENDPOINTS[ApiEndpoint.CreateExpressRequest]
            );
        }
    }
}
