﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Checkout.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace Checkout.Service.Tests
{
    [TestClass()]
    public class CheckoutExpressTest
    {
        Mock<HttpMessageHandler> _mockHttpMessageHandler = new Mock<HttpMessageHandler>();

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid apiKey parameter, expecting a none zero length value"
        )]
        public async Task NullApiKeyInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create(null, null, null, null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid apiKey parameter, expecting a none zero length value"
        )]
        public async Task EmptyApiKeyInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("", null, null, null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid clientId parameter, expecting a none zero length value"
        )]
        public async Task NullClientIdInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("VALID_API_KEY", null, null, null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid clientId parameter, expecting a none zero length value"
        )]
        public async Task EmptyClientIdInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("VALID_API_KEY", "", null, null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid clientSecret parameter, expecting a none zero length value"
        )]
        public async Task NullClientSecretInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("VALID_API_KEY", "VALID_CLIENT_ID", null, null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentException),
            "Invalid clientSecret parameter, expecting a none zero length value"
        )]
        public async Task EmptyClientSecretInCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("VALID_API_KEY", "VALID_CLIENT_ID", "", null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(ArgumentNullException),
            "Invalid payload parameter, expecting a none null value"
        )]
        public async Task NullPayloadCreateMethod()
        {
            CheckoutExpress checkoutExpress = new CheckoutExpress();
            _ = await checkoutExpress.create("VALID_API_KEY", "VALID_CLIENT_ID", "VALID_CLIENT_SECRET", null);
        }

        [TestMethod]
        [ExpectedException(
            typeof(HttpRequestException),
            "Authentication failed. Check your credentials, and try again."
        )]
        public async Task TestAuthRequestNonOkayResponse()
        {
            Payload payload = new Payload();
            CheckoutExpress checkoutExpress = new CheckoutExpress(SupportedEnvironment.Testing);

            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest });

            _ = await checkoutExpress.create(
                "VALID_API_KEY",
                "VALID_CLIENT_ID",
                "VALID_CLIENT_SECRET",
                payload
            );
        }

        [TestMethod]
        [ExpectedException(
            typeof(HttpRequestException),
            "Serialization failed. Invalid or malformed payload. Invalid last name (customer_last_name)!"
        )]
        public async Task TestAuthRequestBadRequest()
        {
            Payload payload = new Payload();
            CheckoutExpress checkoutExpress = new CheckoutExpress(SupportedEnvironment.Testing);
            _ = await checkoutExpress.create(
                "LXCEaNc27bqwYCGMqAjKruEAbSHpBID3",
                "JOHNDOEONLINE_1636532610646",
                "wUBOY0qu9eCbVMjNiAXEAUcKfIExjMBy4ugetwt9d",
                payload
            );
        }

        [TestMethod]
        public async Task TestAuthRequest()
        {
            Payload payload = new Payload();
            payload.msisdn = 254791498482;
            payload.account_number = "3131800000929";
            payload.callback_url = "https://webhook.site/0ccd04f5-a0ad-4b3f-a880-cd89191779e9";
            payload.country_code = "KEN";
            payload.currency_code = "KES";
            payload.customer_email = "Vikash.kumar@cellulant.io";
            payload.customer_first_name = "Vikash";
            payload.customer_last_name = "Kumar";
            payload.due_date = "2024-12-04 11:03:00";
            payload.fail_redirect_url = "https://jsonplaceholder.typicode.com/todos/12";
            payload.merchant_transaction_id = "MT54687019102";
            payload.request_amount = 5000.0;
            payload.request_description = "Bag";
            payload.service_code = "JOHNDOEONLINE";
            payload.success_redirect_url = "https://jsonplaceholder.typicode.com/todos/12";
            payload.prefill_msisdn = false;
            CheckoutExpress checkoutExpress = new CheckoutExpress(SupportedEnvironment.Testing);
            var expressUrls = await checkoutExpress.create(
                "LXCEaNc27bqwYCGMqAjKruEAbSHpBID3",
                "JOHNDOEONLINE_1636532610646",
                "wUBOY0qu9eCbVMjNiAXEAUcKfIExjMBy4ugetwt9d",
                payload
            );

            Assert.IsNotNull(expressUrls.LongUrl);
            Assert.IsNotNull(expressUrls.ShortUrl);
            Console.WriteLine("THE LONG URL: " + expressUrls.LongUrl);
            Console.WriteLine("THE SHORT URLS: " + expressUrls.ShortUrl);
        }
    }
}
