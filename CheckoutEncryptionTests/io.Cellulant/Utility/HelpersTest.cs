﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Checkout.Service.Tests
{
    [TestClass()]
    public class HelpersTests
    {
        [TestMethod]
        [ExpectedException(
            typeof(NullReferenceException),
            "Object reference not set to an instance of an object"
        )]
        public async Task TestPostFormDataNullContent()
        {
            await Helpers.PostFormData("https://example.com", null);
        }

        [TestMethod]
        public async Task TestPostFormData()
        {
            var content = new MultipartFormDataContent();
            content.Add(new StringContent("scope"), "*");
            await Helpers.PostFormData("https://example.com", content);
        }

        [TestMethod]
        public async Task TestPostJsonData()
        {
            await Helpers.PostJsonData("https://example.com", null, null);
            await Helpers.PostJsonData("https://example.com", null, null, null);
        }
    }
}
