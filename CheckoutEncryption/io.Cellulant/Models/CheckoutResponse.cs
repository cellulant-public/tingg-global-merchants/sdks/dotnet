﻿
namespace Checkout.Models
{
    public class CheckoutResponse
    {
        public string encrypted_payload { get; set; }
        public string access_key { get; set; }
    }
}
