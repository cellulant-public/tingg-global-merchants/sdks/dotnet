﻿using System;

namespace Checkout.Models
{
    public class ExpressUrls
    {
        private string longUrl;
        private string shortUrl;

        public string LongUrl
        {
            get { return longUrl; }
            set { longUrl = value; }
        }

        public string ShortUrl
        {
            get { return shortUrl; }
            set { shortUrl = value; }
        }
    }
}

