﻿
using System.ComponentModel.DataAnnotations;

namespace Checkout.Models
{
   public class ChargeBenificiaryDetails
    {
        [Required(ErrorMessage = "charge_beneficiary_code is required in charge_beneficiaries")]
        public string charge_beneficiary_code { get; set; }

        [Required(ErrorMessage = "amount is required in charge_beneficiaries")]
        public double amount { get; set; }
    }
}
