﻿using Checkout.io.Cellulant.Utility;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Models
{
    public class Payload
    {
        [Required(ErrorMessage = "merchant_transaction_id is required.")]
        public string merchant_transaction_id { get; set; }
        public string customer_first_name { get; set; }
        public string customer_last_name { get; set; }

        public bool prefill_msisdn { get; set; }

        [Required(ErrorMessage = "msisdn is required.")]
        public long msisdn { get; set; }


        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
        + "@"
        + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$",
         ErrorMessage = "The customer_email is invalid.")]
        public string customer_email { get; set; }


        [Required(ErrorMessage = "request_amount is required.")]
        public double request_amount { get; set; }


        [Required(ErrorMessage = "currency_code is required.")]
        [StringRange(AllowableValues = new string[] {
            "KES", "NGN", "GHS", "ZMW", "UGX", "TZS", "BWP", "ZAR", "AOA", "MWK",
            "XOF", "XOF", "XAF", "XOF"
        })]
        public string currency_code { get; set; }

        [Required(ErrorMessage = "account_number is required.")]
        public string account_number { get; set; }

        [Required(ErrorMessage = "service_code is required.")]
        public string service_code { get; set; }

        [FutureUtcDateTime(ErrorMessage = "Invalid date format or not a future UTC date.")]
        public string due_date { get; set; }

        public string request_description { get; set; }

        [Required(ErrorMessage = "country_code is required.")]
        [StringRange(AllowableValues = new string[] {
            "KEN", "NGA", "GHA", "ZMB", "UGA", "TZA", "BWA", "ZAF", "AGO", "MWI",
            "CIV", "SEN", "CMR", "BFA"
        })]
        public string country_code { get; set; }

        [StringRange(AllowableValues = new string[] {"ar", "en", "pt", "fr"})]
        public string language_code { get; set; }
        public string payment_option_code { get; set; }

        [Required(ErrorMessage = "success_redirect_url is required.")]
        [Url(ErrorMessage = "The success_redirect_url should be a valid URL")]
        public string success_redirect_url { get; set; }

        [Required(ErrorMessage = "fail_redirect_url is required.")]
        [Url(ErrorMessage = "The fail_redirect_url should be a valid URL")]
        public string fail_redirect_url { get; set; }

        [Url(ErrorMessage = "The pending_redirect_url should be a valid URL")]
        public string pending_redirect_url { get; set; }

        [Required(ErrorMessage = "callback_url is required.")]
        [Url(ErrorMessage = "The callback_url should be a valid URL")]
        public string callback_url { get; set; }
        public List<ChargeBenificiaryDetails> charge_beneficiaries { get; set; }
    }
}
