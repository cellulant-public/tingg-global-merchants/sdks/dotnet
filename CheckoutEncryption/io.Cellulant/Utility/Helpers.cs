﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Checkout.Service
{
    public class Helpers
    {

        public static JObject StringToJson(string jsonString)
        {
            return JObject.Parse(jsonString);
        }

        public async static Task<HttpResponseMessage> PostFormData(string url, MultipartFormDataContent content)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "multipart/form-data");
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri(url);
            request.Content = content;
            var header = new ContentDispositionHeaderValue("form-data");
            request.Content.Headers.ContentDisposition = header;

            var response = await client.PostAsync(request.RequestUri.ToString(), request.Content);
            return response;
        }

        public async static Task<HttpResponseMessage> PostJsonData(string url, string apiKey, object payload)
        {
            var client = new HttpClient();
            var content = JsonConvert.SerializeObject(payload);
            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            byteContent.Headers.Add("apiKey", apiKey);

            var response = await client.PostAsync(url, byteContent);
            return response;
        }

        public async static Task<HttpResponseMessage> PostJsonData(string url, string apiKey, object payload, string accessToken)
        {
            var client = new HttpClient();
            var content = JsonConvert.SerializeObject(payload);
            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            byteContent.Headers.Add("apiKey", apiKey);

            var response = await client.PostAsync(url, byteContent);
            return response;
        }

    }
}
