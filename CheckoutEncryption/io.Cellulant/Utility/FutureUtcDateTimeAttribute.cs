﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Checkout.io.Cellulant.Utility
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    internal class FutureUtcDateTimeAttribute: ValidationAttribute
    {
        public FutureUtcDateTimeAttribute()
        {
            ErrorMessage = "The {0} field must be a valid future UTC date in the format 'YYYY-MM-DD HH:mm:ss'.";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is string dateString)
            {
                if (DateTime.TryParseExact(dateString, "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out DateTime parsedDateTime))
                {
                    // Check if the parsed date is a future UTC date
                    if (parsedDateTime > DateTime.UtcNow)
                    {
                        return ValidationResult.Success;
                    }
                }
            }

            return new ValidationResult(string.Format(ErrorMessage, validationContext.MemberName));
        }
    }
}
