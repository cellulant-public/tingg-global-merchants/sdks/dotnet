﻿using Checkout.Models;
using System;
using System.Text.Json;

namespace Checkout.Service
{
   public class CheckoutEncryption
    {

        /** The iv key. */
        private readonly string ivKey;

        /** The secret key. */
        private readonly string secretKey;

        /** The encryption. */
        private readonly Encryption encryption;

        public CheckoutEncryption(string ivKey, string secretKey)
        {
            // this.accessKey = accessKey;
            this.ivKey = ivKey;
            this.secretKey = secretKey;
            encryption = new Encryption(this.ivKey, this.secretKey);
        }

        public string Encrypt(Payload payload)
        {
            try
            {
                String jsonPayload = JsonSerializer.Serialize(payload);
                return Encrypt(jsonPayload);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public String Encrypt(String jsonPayload)
        {
            try
            {
                String encryptedPayload = encryption.Encrypt(jsonPayload);

                return encryptedPayload;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
