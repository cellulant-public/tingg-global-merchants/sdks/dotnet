﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Checkout.Service
{
    internal class Encryption
    {

        private readonly string ivKey;
        private readonly string secretKey;

        internal Encryption(string ivKey, string secretKey)
        {
            this.ivKey = ivKey;
            this.secretKey = secretKey;
        }


        private Aes GetAes()
        {
            try { 
            HashAlgorithm hash = SHA256.Create();

            var hashedIV = hash.ComputeHash(UTF8Encoding.UTF8.GetBytes(ivKey));
            var hashedSecret = hash.ComputeHash(UTF8Encoding.UTF8.GetBytes(secretKey));
            var iv = Encoding.Default.GetBytes((BitConverter.ToString(hashedIV).Replace("-", string.Empty).ToLower()[..16]));
            var secret = Encoding.ASCII.GetBytes((BitConverter.ToString(hashedSecret).Replace("-", string.Empty).ToLower()[..32]));

            Aes crypt = Aes.Create("AES");
            crypt.Key = secret;
            crypt.IV = iv;
            crypt.Mode = CipherMode.CBC;
            crypt.Padding = PaddingMode.PKCS7;

            return crypt;
            }
            catch (Exception e)
            {
                Console.WriteLine($"\tMessage: {e.Message}");
                throw;
            }

        }


        private byte[] Encrypt(byte[] plainBytes, Aes aesManaged)
        {
            try { 
            byte[] encrypted = aesManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);

            return Encoding.Default.GetBytes(Convert.ToBase64String(encrypted));
            }
            catch (Exception e)
            {
                Console.WriteLine($"\tMessage: {e.Message}");
                throw;
            }
        }


        public string Encrypt(string plainText)
        {
            try { 
            var plainBytes = Encoding.Default.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetAes()));
            }
            catch (Exception e)
            {
                Console.WriteLine($"\tMessage: {e.Message}");
                throw;
            }
        }



    }


}

