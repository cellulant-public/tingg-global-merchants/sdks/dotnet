using System;
using System.Net;
using System.Net.Http;
using Checkout.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Checkout.Service
{
    public class CheckoutExpress
    {
        private readonly SupportedEnvironment environment;

        public CheckoutExpress(SupportedEnvironment environment = SupportedEnvironment.Sandbox)
        {
            this.environment = environment;
        }

        public async Task<ExpressUrls> create(
            string apiKey,
            string clientId,
            string clientSecret,
            Payload payload
        )
        {
            if (apiKey == null || apiKey.Trim() == "")
            {
                throw new ArgumentException(
                    "Invalid apiKey parameter, expecting a none zero length value"
                );
            }
            if (clientId == null || clientId.Trim() == "")
            {
                throw new ArgumentException(
                    "Invalid clientId parameter, expecting a none zero length value"
                );
            }
            if (clientSecret == null || clientSecret.Trim() == "")
            {
                throw new ArgumentException(
                    "Invalid clientSecret parameter, expecting a none zero length value"
                );
            }
            if (payload == null)
            {
                throw new ArgumentNullException(
                    "Invalid payload parameter, expecting a none null value"
                );
            }

            var authPayload = new Dictionary<String, String>();
            authPayload.Add("client_id", clientId);
            authPayload.Add("client_secret", clientSecret);
            authPayload.Add("grant_type", "client_credentials");
            var httpResponse = await Helpers.PostJsonData(
                Constants.AUTH_BASE_URL[this.environment]
                    + Constants.API_ENDPOINTS[ApiEndpoint.Auth],
                apiKey,
                authPayload
            );
            var responseContent = httpResponse.Content.ReadAsStringAsync().Result;
            var jsonResponse = Helpers.StringToJson(responseContent);
            var accessToken = jsonResponse.GetValue("access_token");
            if (httpResponse.StatusCode != HttpStatusCode.OK || accessToken == null)
            {
                throw new HttpRequestException(
                    "Authentication failed. Check your credentials, and try again."
                );
            }
            var accessTokenString = accessToken.ToString();

            httpResponse = await Helpers.PostJsonData(
                Constants.API_BASE_URL[this.environment]
                    + Constants.API_ENDPOINTS[ApiEndpoint.CreateExpressRequest],
                apiKey,
                payload,
                accessTokenString
            );
            responseContent = httpResponse.Content.ReadAsStringAsync().Result;
            jsonResponse = Helpers.StringToJson(responseContent);
            var status = jsonResponse.GetValue("status");
            var message = jsonResponse.GetValue("message");
            var results = jsonResponse.GetValue("results");
            var statusCode = jsonResponse.GetValue("statusCode");
            if (
                httpResponse.StatusCode != HttpStatusCode.OK
                || (status == null && statusCode == null)
            )
            {
                if (httpResponse.StatusCode == HttpStatusCode.BadRequest)
                    throw new HttpRequestException(
                        "Serialization failed. Invalid or malformed payload. " + message
                    );
                if (httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    throw new HttpRequestException(
                        "Authentication failed. Invalid or expired token. Try again later."
                    );
                throw new HttpRequestException(
                    "Request to create the express url failed with the status code: "
                        + httpResponse.StatusCode
                        + ", Status: "
                        + status
                        + ", Message: "
                        + message
                );
            }
            if (statusCode == null)
            {
                statusCode = status["status_code"].ToString();
            }
            statusCode = statusCode.ToString();

            if ("132".Equals(statusCode))
                throw new HttpRequestException(
                    "Authentication failed. Invalid or expired token. Try again later."
                );
            if ("400".Equals(status))
                throw new HttpRequestException(
                    "Serialization failed. Invalid or malformed JSON payload."
                );
            if (Constants.PAYLOAD_FIELDS_ERROR_CODES.Contains(status.ToString()))
                throw new HttpRequestException("Validation failed. " + message.ToString());

            return new ExpressUrls
            {
                LongUrl = results["long_url"].ToString(),
                ShortUrl = results["short_url"].ToString()
            };
        }
    }
}
