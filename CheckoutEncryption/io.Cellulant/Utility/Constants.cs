﻿using System;
using System.Collections.Generic;

namespace Checkout.Service
{

    public enum SupportedEnvironment
    {
        Testing,
        Sandbox,
        Production
    }

    public enum ApiEndpoint
    {
        Auth,
        CreateExpressRequest
    }

    public class Constants
    {

        public static IDictionary<SupportedEnvironment, string> AUTH_BASE_URL = new Dictionary<SupportedEnvironment, string>() {
            {SupportedEnvironment.Production, "https://api.tingg.africa/v1"},
            {SupportedEnvironment.Testing, "https://api-test.tingg.africa/v1"},
            {SupportedEnvironment.Sandbox, "https://api-approval.tingg.africa/v1"},
        };

        public static IDictionary<SupportedEnvironment, string> API_BASE_URL = new Dictionary<SupportedEnvironment, string>() {
            {SupportedEnvironment.Production, "https://api.tingg.africa/v1"},
            {SupportedEnvironment.Sandbox, "https://api-approval.tingg.africa/v1"},
            {SupportedEnvironment.Testing, "https://online.uat.tingg.africa/testing"},
        };

        public static IDictionary<ApiEndpoint, string> API_ENDPOINTS = new Dictionary<ApiEndpoint, string>() {
            {ApiEndpoint.Auth, "/oauth/token/request"},
            {ApiEndpoint.CreateExpressRequest, "/request-service/checkout-request/express-request"},
        };

        public static List<string> PAYLOAD_FIELDS_ERROR_CODES = new List<string>() {
            "7",
            "400",
            "1018",
            "1019",
            "1029",
            "1028",
            "1026",
            "1014",
            "1017",
            "1003",
            "1007",
            "1003",
            "1008",
            "1003",
            "1012",
            "1013",
            "1015",
            "1030",
        };

    }
}
