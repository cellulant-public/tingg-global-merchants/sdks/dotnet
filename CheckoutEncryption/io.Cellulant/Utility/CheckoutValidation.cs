﻿using Checkout.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;

namespace Checkout.io.Cellulant.Utility
{
    internal class ValidationError
    {
        public string field { get; set;  }
        public string message { get; set; }

        public ValidationError(string field, string message)
        {
            this.field = field != string.Empty ? field : null;
            this.message = message;
        }
    }
    internal class ValidationResultModel
    {
        public List<ValidationError> errors { get; }

        public ValidationResultModel(List<ValidationResult> validationResults)
        {
            errors = validationResults
                    .Select(result => new ValidationError(result.MemberNames.FirstOrDefault(), result.ErrorMessage))
                    .Where(error => error.field != null)  // Exclude global errors
                    .ToList();
        }
    }
    public class CheckoutValidation
    {
        public static Payload Validate(Payload payload)
        {
            var validationResults = ValidateObject(payload);
            if (validationResults != null) {
                //has error messages
                var validationResultModel = new ValidationResultModel(validationResults);
                if (validationResultModel.errors.Count == 0) {
                    return payload;
                }
                throw new ValidationException(JsonSerializer.Serialize(validationResultModel));
            }
            else
            {
                return payload;
            }
            
        }
        private static List<ValidationResult> ValidateObject(object obj)
        {
            var validationContext = new ValidationContext(obj, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            // Try to validate the object based on data annotations
            if (Validator.TryValidateObject(obj, validationContext, validationResults, validateAllProperties: true))
            {
                //valid
                return null;
            }
            else
            {
                //invalid payload
                return validationResults;
            }
        }       
    }
}
