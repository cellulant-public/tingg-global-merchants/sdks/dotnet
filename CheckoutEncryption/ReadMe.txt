﻿Tingg Checkout SDK ReadMe file
=============================================================
1. To use this library add a using statement for Checkout.Models and Checkout.Service
2. You can now initialize CheckoutEncryption  passing in the relevant parameters IVKey and SecretKey (advised to
   have these keys in a separate configuration source)
3. Call CheckoutValidation.Validate() to validate the payload before encrypting.
3. Call Encrypt() on the CheckoutEncryption instance passing in the Payload variable.